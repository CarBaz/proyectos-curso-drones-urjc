# -*- coding: utf-8 -*-
#
# Written by: Carlos Bazaga
#
# Licensed under BSD 2-Clause
#

import cv2
import numpy as np
from PID import PID
from math import sqrt
import quaternionsMath as qm

# HUD Settings.
T_FONT  = cv2.FONT_HERSHEY_PLAIN
T_SCALE = 1
T_COLOR = (255, 255, 255)

class MyAlgorithm():
    """AI module."""
    def __init__(self, drone):
        """Constructor."""
        self.drone    = drone
        self.minError = 8
        self.minActit = 0.01
        self.target_X = 125     # Camera Relative Center X
        self.target_Y = 125     # Camera Relative Center Y
        self.target_Z = 1.5     # Desired operation altitude.
        self.near_rng = 80
        self.far_rng  = 100
        self.reset()

    def reset(self):# X & Y PID tunning by Ziegler–Nichols method.
        """Reset PIDs values."""
        self.xPid = PID(Epsilon=self.minError, Kp=0.01, Ki=0.02, Kd=0.003, Imax=5, Imin=-5)
        self.yPid = PID(Epsilon=self.minError, Kp=0.01, Ki=0.02, Kd=0.003, Imax=5, Imin=-5)
        self.zPid = PID(Epsilon=self.minActit, Kp=0.50, Ki=0.00, Kd=0.000, Imax=5, Imin=-5)
        self.aPid = PID(Epsilon=self.minActit, Kp=0.20, Ki=0.00, Kd=0.000, Imax=5, Imin=-5)

    def where_am_i(self):
        """Return drone coordinates."""
        # Who cares?
        pose = self.drone.getPose3D()
        return pose.x, pose.y, pose.z

    def whats_my_attitude(self):
        """Return drone attitude."""
        # Always "Bad Attitude" Baracus.
        pose = self.drone.getPose3D()
        return qm.quat2Actitude(pose.q0, pose.q1, pose.q2, pose.q3)

    def _print_HUD(self, image, coords, *args):
        """Overlay args strings on the given image starting from coords."""
        y, x = coords
        for data in args:
            cv2.putText(image, data, (y+15, x+15), T_FONT, T_SCALE, T_COLOR)
            x += 15 * T_SCALE

    def execute(self):
        """Detect the turtle on the drone camera input and pursue it."""
        # Obtain color filter values from GUI
        FilterValues = self.drone.getColorFilterValues()
        hmin = FilterValues.getHMin()
        hmax = FilterValues.getHMax()
        vmin = FilterValues.getSMin()
        vmax = FilterValues.getSMax()
        smin = FilterValues.getVMin()
        smax = FilterValues.getVMax()

        # Hardcoded color filter: GREEN.
        hmin, hmax, vmin, vmax, smin, smax = 50, 70, 20, 235, 10, 245

        # Get image from camera.
        inputImage      = self.drone.getImage()

        # Soften the image.
        softenedImage   = cv2.GaussianBlur(inputImage, (5, 5), 0)

        # Translate the image to the HSV space.
        hsvImage        = cv2.cvtColor(softenedImage, cv2.COLOR_BGR2HSV)

        # Filter the HSV image with the color filter setted previously.
        thresoldImage   = cv2.inRange(hsvImage, np.array([hmin, vmin, smin]), np.array([hmax, vmax, smax]))

        # Extract contours from filtered image.
        thresoldCopy        = np.copy(thresoldImage)
        contours, hierarchy = cv2.findContours(thresoldCopy, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        # Draw contours detected overlaid on the original image.
        imageCopy = np.copy(inputImage)
        cv2.drawContours(imageCopy, contours, -1, (255,255,0), 2)

        # Draw image's "bull's eye".
        image_center = self.target_X, self.target_Y
        cv2.circle(imageCopy, image_center, self.minError,(255,125, 0),2)
        cv2.circle(imageCopy, image_center, 1,(255,125, 0),2)

        # Draw near and far flight area circles.
        # (Inside near circle will try to flight at target_z altitude)
        # (between both circles will keep altitude)
        # (Outside far circle will climb)
        cv2.circle(imageCopy, image_center, self.near_rng,(0, 0, 255),1)
        cv2.circle(imageCopy, image_center, self.far_rng,(0, 255, 255),1)

        # Locate turtle on the image.
        if contours:
            cnt = contours[0] # We asume there is only one Green Target on the scene.

            # Locate the minimal circle enclosing the selected contour.
            (x, y), radius = cv2.minEnclosingCircle(cnt)

            # The center of that circle will be considered the center of the turtle.
            center = (int(x), int(y))
            # Radius could be used to determine distance to a given known size turtle.
            radius = int(radius)

            # Draw a "bull's eye" on the turtle.
            cv2.circle(imageCopy, center, radius,(255,0, 0),2)
            cv2.circle(imageCopy, center, 1,(255,0, 0),2)

            # Strafe control
            x_error = self.target_X - center[0]
            y_error = self.target_Y - center[1]
            xp, xi, xd = self.xPid.process(x_error)
            yp, yi, yd = self.yPid.process(y_error)

            # Altitude control
            distance   = sqrt((x_error)*(x_error) + (y_error)*(y_error))
            print "distance: ", distance
            if distance > self.far_rng:     # If too far from turtle increase altitude.
                zp, zi, zd = 1, 0, 0
            elif distance > self.near_rng:  # If mid far from turtle keep altitude.
                zp, zi, zd = 0, 0, 0
            else:                           # If close to turtle low flight.
                mz = self.where_am_i()[2]
                zp, zi, zd = self.zPid.process(self.target_Z-mz)

        else: # Keep an stationary ascendant flight until turtle is detected.
            # SEARCH MODE
            zp, zi, zd = 1.5, 0, 0
            xp, xi, xd = 0, 0, 0
            yp, yi, yd = 0, 0, 0

        # Attitude control
        ap, ay, ar = self.whats_my_attitude()
        ap, ai, ad = self.aPid.process(-ay)

        # Accumulate PIDs outputs.
        xvel = xp + xi + xd
        yvel = yp + yi + yd
        zvel = zp + zi + zd
        avel = ap + ai + ad

        # Actuate.
        self.drone.sendCMDVel(xvel, yvel, zvel, avel, 0, 0)

        # Print HUD
        if contours:
            relative_center = (-x_error, -y_error)
            self._print_HUD(imageCopy, center, "{0}".format(relative_center))
        else:
            self._print_HUD(imageCopy, (70, 90), "SEARCHING")

        # Update GUI images.
        self.drone.setThresoldImage(thresoldImage)
        self.drone.setTrackImage(imageCopy)
