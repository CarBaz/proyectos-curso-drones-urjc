# -*- coding: utf-8 -*-
#
# Written by: Carlos Bazaga
#
# Licensed under BSD 2-Clause
#

import jderobot
from PID import PID
from math import sqrt
from Beacon import Beacon
import quaternionsMath as qm

class MyAlgorithm:
    """AI module."""
    def __init__(self, drone):
        """Constructor."""
        self.drone    = drone
        self.minError = 0.01
        self.range    = 0.02
        self.target_Z = 1       # Desired operation altitude.
        self.reset()

    def init_bacon(self):
        """Set beacons coordinates array."""
        # Bacon cooking.
        self.beacons = []
        self.beacons.append(Beacon('baliza1', jderobot.Pose3DData( 0.0,  5.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0), False, False))
        self.beacons.append(Beacon('baliza2', jderobot.Pose3DData( 5.0,  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0), False, False))
        self.beacons.append(Beacon('baliza3', jderobot.Pose3DData( 0.0, -5.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0), False, False))
        self.beacons.append(Beacon('baliza4', jderobot.Pose3DData(-5.0,  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0), False, False))
        self.beacons.append(Beacon('baliza5', jderobot.Pose3DData(10.0,  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0), False, False))
        self.beacons.append(Beacon('inicio' , jderobot.Pose3DData( 0.0,  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0), False, False))

    def reset(self):
        """Reset PIDs values."""
        self.xPid = PID(Epsilon=self.minError, Kp=3.00, Ki=0.15, Kd=4.50, Imax=5, Imin=-5)
        self.yPid = PID(Epsilon=self.minError, Kp=3.00, Ki=0.15, Kd=4.50, Imax=5, Imin=-5)
        self.zPid = PID(Epsilon=self.minError, Kp=2.00, Ki=0.00, Kd=0.00, Imax=5, Imin=-5)
        self.aPid = PID(Epsilon=self.minError, Kp=0.20, Ki=0.00, Kd=0.00, Imax=5, Imin=-5)
        self.init_bacon()

    def get_next_bacon(self):
        """Return first non reached beacon.

            In case they all has been reached set all as 'not reached'
            and return the first one.
        """
        # Yum, yum...
        for beacon in self.beacons:
            if not beacon.isReached():
                return beacon
        for beacon in self.beacons:
            beacon.setReached(False)
        return self.beacons[0]

    def where_am_i(self):
        """Return drone coordinates."""
        # Who cares?
        pose = self.drone.getPose3D()
        return pose.x, pose.y, pose.z

    def where_is_the_bacon(self):
        """Return actual Bacon coordinates."""
        # I want bacon!
        pose = self.actualBeacon.getPose()
        return pose.x, pose.y, pose.z

    def whats_my_attitude(self):
        """Return drone attitude."""
        # Always "Bad Attitude" Baracus.
        pose = self.drone.getPose3D()
        return qm.quat2Actitude(pose.q0, pose.q1, pose.q2, pose.q3)

    def execute(self):
        """Navigate to the next Bacon."""
        # Crispy and sweet Bacon...
        self.actualBeacon = self.get_next_bacon()
        if self.actualBeacon:

            # Get actual situation.
            mx, my, mz = self.where_am_i()
            bx, by, bz = self.where_is_the_bacon()
            ap, ay, ar = self.whats_my_attitude()
            distance   = sqrt((bx-mx)*(bx-mx) + (by-my)*(by-my))

            # Print out actual situation.
            print("Bacons reached:\t\t {0}\n\n".format([beacon.isReached() for beacon in self.beacons]))
            print("I'm at:\t\t\t ({0:=20.16f}, {1:=20.16f}, {2:=20.16f})".format(mx, my, mz))
            print("Heading at:\t\t ({0:=20.16f}, {1:=20.16f}, {2:=20.16f})".format(ap, ay, ar))
            print("The bacon is at:\t ({0:=20.16f}, {1:=20.16f}, {2:=20.16f})".format(bx, by, bz))
            print("Error:\t\t\t ({0:=20.16f}, {1:=20.16f}, {2:=20.16f})".format((bx-mx), (by-my), (1-mz)))
            print("\nDistance:\t\t\t {0}\n\n".format(distance))

            # Get PIDs suggested actuations.
            xp, xi, xd = self.xPid.process(bx-mx)
            yp, yi, yd = self.yPid.process(by-my)
            zp, zi, zd = self.zPid.process(self.target_Z-mz)
            ap, ai, ad = self.aPid.process(-ay)

            # Print PIDs suggested actuations.
            print("X PID:\t\t\t ({0:=20.16f}, {1:=20.16f}, {2:=20.16f})".format(xp, xi, xd))
            print("Y PID:\t\t\t ({0:=20.16f}, {1:=20.16f}, {2:=20.16f})".format(yp, yi, yd))
            print("Z PID:\t\t\t ({0:=20.16f}, {1:=20.16f}, {2:=20.16f})".format(zp, zi, zd))
            print("A PID:\t\t\t ({0:=20.16f}, {1:=20.16f}, {2:=20.16f})".format(ap, ai, ad))

            # Accumulate PIDs outputs.
            xvel = xp + xi + xd
            yvel = yp + yi + yd
            zvel = zp + zi + zd
            avel = ap + ai + ad

            if distance <= self.range: # Bacon reached :-P
                print "\nREACHED!!\n"
                # Reset PIDs and iterate bacon.
                self.xPid.reset()
                self.yPid.reset()
                self.actualBeacon.setReached(True)
                self.actualBeacon = self.get_next_bacon()
                bx, by, bz = self.where_is_the_bacon()

            else:
                print "\nSo my order is:\t\t ({0:=20.16f}, {1:=20.16f}, {2:=20.16f}, {3:=20.16f}, {4:=20.16f}, {5:=20.16f})\n".format(xvel, yvel, zvel, avel, 0, 0)
                # Actuate Drone.
                self.drone.sendCMDVel(yvel, xvel, zvel, avel, 0, 0) # The sensor.sendCMDVel function inverts X-Y values.
