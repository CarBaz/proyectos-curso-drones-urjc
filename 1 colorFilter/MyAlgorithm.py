# -*- coding: utf-8 -*-
#
# Written by: Carlos Bazaga
#
# Licensed under BSD 2-Clause
#

import cv2
import numpy as np

# Extra windows settings.
    # May result on instabilities because conflicts between OpenCV and Qt Widows.
    # Avoid activation of "color Filter" component if set to true, this will need one of the harcoded filters to be setted.
SHOW_STEPS = True

# HUD Settings.
T_FONT  = cv2.FONT_HERSHEY_PLAIN
T_SCALE = 1
T_COLOR = (255, 255, 255)

class MyAlgorithm():
    """AI module."""
    def __init__(self, drone):
        """Constructor."""
        self.drone = drone

    def _print_HUD(self, image, *args):
        """Overlay args strings on the given image."""
        baseline = 15
        for data in args:
            cv2.putText(image, data, (5, baseline), T_FONT, T_SCALE, T_COLOR)
            baseline += 15 * T_SCALE

    def execute(self):
        """Detect a ball on the drone camera input."""
        # Obtain color filter values from GUI
        FilterValues = self.drone.getColorFilterValues()
        hmin = FilterValues.getHMin()
        hmax = FilterValues.getHMax()
        vmin = FilterValues.getSMin()
        vmax = FilterValues.getSMax()
        smin = FilterValues.getVMin()
        smax = FilterValues.getVMax()

        # Hardcoded color filter: RED.
        hmin, hmax, vmin, vmax, smin, smax = 110, 180, 140, 255, 155, 255

        # Hardcoded color filter: RED & BLUE.
        # hmin, hmax, vmin, vmax, smin, smax = 0, 159, 107, 253, 57, 255

        # Get image from camera.
        inputImage    = self.drone.getImage()

        # Soften the image.
        softenedImage = cv2.GaussianBlur(inputImage, (5, 5), 0)

        # Translate the image to the HSV space.
        hsvImage      = cv2.cvtColor(softenedImage, cv2.COLOR_BGR2HSV)

        # Filter the HSV image with the color filter setted previously.
        thresoldImage = cv2.inRange(hsvImage, np.array([hmin, vmin, smin]), np.array([hmax, vmax, smax]))

        # Extract contours from filtered image.
        thresoldCopy        = np.copy(thresoldImage)
        contours, hierarchy = cv2.findContours(thresoldCopy, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        # Draw contours detected overlaid on the original image.
        imageCopy = np.copy(inputImage)
        cv2.drawContours(imageCopy, contours, -1, (0,255,0), 3)

        # Show process steps on stand alone windows.
        if SHOW_STEPS: # (Correction from BGR to RGB needed.)
            cv2.imshow('Input Image',               cv2.cvtColor(inputImage, cv2.COLOR_BGR2RGB))
            cv2.imshow('Softened Image',            cv2.cvtColor(softenedImage, cv2.COLOR_BGR2RGB))
            cv2.imshow('HSV Image (False color)',   hsvImage)       # as it's false color correction of R-B is useless
            cv2.imshow('Filtered Image',            thresoldImage)  # as it's B/W image correction of R-B is useless
            cv2.imshow('Contours Image',            cv2.cvtColor(imageCopy, cv2.COLOR_BGR2RGB))

        # Attempt to search circular shapes. (All this process can be greatly improved)
        pattern = np.zeros((120,120,1), np.uint8)                                                           # Set a black image.
        cv2.circle(pattern, (60,60), 50,(255, 255, 255),-1)                                                 # Draw a white circe.
        c_contours, c_hierarchy = cv2.findContours(pattern, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)     # Extract circle contour.
        best_contour = None
        min_diff = 1
        if contours:
            for contour in contours: # Search for the best matching shape in detected contours.
                diff = cv2.matchShapes(c_contours[0], contour, 3, 0.)
                if diff < min_diff and diff != 0.:
                    min_diff     = diff
                    best_contour = contour
            print("Best match: {0}".format(min_diff))

        # Locate the ball on the image.
        center = None
        if contours:
            if best_contour == None: # In case shape matching failed or not executed.
                best_contour = contours[0]

            # Locate the minimal circle enclosing the selected contour.
            (x, y), radius = cv2.minEnclosingCircle(best_contour)

            # The center of that circle will be considered the center of the detected ball.
            center = (int(x), int(y))
            # Radius could be used to determine distance to a given known size ball.
            radius = int(radius)

            # Draw a "bull's eye" on the ball.
            cv2.circle(imageCopy, center, radius,(0,0, 255),2)
            cv2.circle(imageCopy, center, 1,(0,0, 255),2)

        # Print HUD
        if center:
            self._print_HUD(imageCopy, "Ball center: {0}".format(center))
        else:
            self._print_HUD(imageCopy, "Ball center: {0}".format("OOS"))

        # Show final step on a stand alone window.
        if SHOW_STEPS: # (Correction from BGR to RGB needed.)
            cv2.imshow('Final Image', cv2.cvtColor(imageCopy, cv2.COLOR_BGR2RGB))

        # Update GUI images.
        self.drone.setThresoldImage(thresoldImage)
        self.drone.setTrackImage(imageCopy)
