# -*- coding: utf-8 -*-
#
# Written by: Carlos Bazaga
#
# Licensed under BSD 2-Clause
#

import cv2
import numpy as np
from PID import PID
import quaternionsMath as qm

# Flight mode settings.
    # FPV mode is a rudimentary algorithm and may take strange decisions.
FPV_MODE = False

# HUD Settings.
T_FONT  = cv2.FONT_HERSHEY_PLAIN
T_SCALE = 1
T_COLOR = (255, 255, 255)

class MyAlgorithm():
    """AI module."""
    def __init__(self, drone):
        """Constructor."""
        self.drone    = drone
        self.minError = 10
        self.minActit = 0.5
        self.minAltit = 0.01
        if FPV_MODE:
            self.target_X = 145 # Camera Relative Center X
            self.target_Y = 125 # Camera Relative Center Y
            self.target_Z = 0.5 # Desired operation altitude.
        else:
            self.target_X = 125 # Camera Relative Center X
            self.target_Y = 125 # Camera Relative Center Y
            self.target_Z = 2.5 # Desired operation altitude.
        self.reset()

    def reset(self): # X PID tunning by Ziegler–Nichols method.
        """Reset PIDs values."""
        self.xPid = PID(Epsilon=self.minError, Kp=0.01, Ki=0.01, Kd=0.003, Imax=5, Imin=-5)
        self.zPid = PID(Epsilon=self.minAltit, Kp=2.00, Ki=0.00, Kd=0.000, Imax=5, Imin=-5)
        self.aPid = PID(Epsilon=self.minActit, Kp=0.02, Ki=0.02, Kd=0.003, Imax=5, Imin=-5)

    def where_am_i(self):
        """Return drone coordinates."""
        # Who cares?
        pose = self.drone.getPose3D()
        return pose.x, pose.y, pose.z

    def _print_HUD(self, image, coords, *args):
        """Overlay args strings on the given image starting from coords."""
        baseline = 15
        y, x = coords
        for data in args:
            cv2.putText(image, data, (y+15, x+15), T_FONT, T_SCALE, T_COLOR)
            x += 15 * T_SCALE

    def execute(self):
        """Detect the road and follow it."""
        # Obtain color filter values from GUI
        FilterValues = self.drone.getColorFilterValues()
        hmin = FilterValues.getHMin()
        hmax = FilterValues.getHMax()
        vmin = FilterValues.getSMin()
        vmax = FilterValues.getSMax()
        smin = FilterValues.getVMin()
        smax = FilterValues.getVMax()

        # Hardcoded color filter: GREY.
        hmin, hmax, vmin, vmax, smin, smax = 90, 97, 0, 50, 45, 80

        # Get image from camera.
        inputImage      = self.drone.getImage()

        # Soften the image.
        softenedImage   = cv2.bilateralFilter(inputImage,9,75,75)

        # Translate the image to the HSV space.
        hsvImage        = cv2.cvtColor(softenedImage, cv2.COLOR_BGR2HSV)

        # Filter the HSV image with the color filter setted previously.
        thresoldImage   = cv2.inRange(hsvImage, np.array([hmin, vmin, smin]), np.array([hmax, vmax, smax]))

        # Extract contours from filtered image.
        thresoldCopy        = np.copy(thresoldImage)
        contours, hierarchy = cv2.findContours(thresoldCopy, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        # Draw contours detected overlaid on the original image.
        imageCopy = np.copy(inputImage)
        cv2.drawContours(imageCopy, contours, -1, (255,255,0), 2)

        # Draw image's "bull's eye".
        image_center = self.target_X, self.target_Y
        cv2.circle(imageCopy, image_center, self.minError,(255,125, 0),2)
        cv2.circle(imageCopy, image_center, 1,(255,125, 0),2)

        # Locate road on the image.
        if contours:
            # We asume the main road is the biggest contour.
            best_contour = max(contours, key=cv2.contourArea)

            # We are going to use a circle to determine the middle of the road
            # and an ellipse to determine te orientation.
            try:
                # Locate the minimal circle enclosing the selected contour.
                (x, y), radius = cv2.minEnclosingCircle(best_contour)

                # The center of that circle will be considered the center of the detected ball.
                center = (int(x), int(y))

                # Draw a "dot" on the middle of the road.
                cv2.circle(imageCopy, center, 1, (255,0, 0), 2)

                # Locate the best fitting ellipse for the selected contour.
                ellipse = cv2.fitEllipse(best_contour)

                # Draw a the ellipse on the road.
                cv2.ellipse(imageCopy, ellipse, (255,0,0), 2)
            except:
                return

            # Altitude control
            mz = self.where_am_i()[2]
            zp, zi, zd = self.zPid.process(self.target_Z - mz)

            # Attitude control
            #   We aim to keep aligned with the road based on the ellipse inclination.
            inclination = ellipse[2]
            #   Ellipse inclination is composed by two (0, 180) degrees ranges,
            #   we need to translate to one (-180, 180) range for zenithal mode
            #   and to one (-90, 90) range for FPV mode.
            if FPV_MODE:
                y_angle = inclination -90
            elif inclination < 90:
                y_angle = -inclination
            else:
                y_angle = 180 - inclination
            ap, ai, ad = self.aPid.process(y_angle)

            # Strafe control
            x_error    = self.target_X - center[0]
            xp, xi, xd = self.xPid.process(x_error)

            # Speed control
            #   We do not use a PID this time.
            #   A function relative to distance to the middle
            #   of the road and curvature (ellipse inclination) will work.
            #
            #   We use a negative exponential to moderate speed based on distance. (We cut out negative speeds)
            speed = max((self.target_X - pow(1.09, abs(x_error)))/125., 0)

        else:
            if FPV_MODE: # We turn until we see the road.
                xp, xi, xd = 0, 0, 0
                zp, zi, zd = 0, 0, 0
                ap, ai, ad = 0.2, 0, 0
                speed = 0
            else: # We climb until we see the road.
                xp, xi, xd = 0, 0, 0
                zp, zi, zd = 1, 0, 0
                ap, ai, ad = 0, 0, 0
                speed = 0

        # Accumulate PIDs outputs.
        xvel = xp + xi + xd
        zvel = zp + zi + zd
        avel = ap + ai + ad

        # Actuate.
        self.drone.sendCMDVel(xvel, speed, zvel, avel, 0, 0)

        # Print HUD
        if contours:
            self._print_HUD(imageCopy, (-10,0),
                            "Distance: {0}".format(x_error),
                            "Speed: {0}".format(speed),
                            "",
                            "Angle: {0}".format(y_angle),
                            "YawDPS: {0}".format(avel))
        else:
            self._print_HUD(imageCopy, (70, 90), "SEARCHING")

        # Update viewports.
        self.drone.setThresoldImage(thresoldImage)
        self.drone.setTrackImage(imageCopy)
