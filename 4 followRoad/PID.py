# -*- coding: utf-8 -*-
#
# Written by: Carlos Bazaga
#
# Licensed under BSD 2-Clause
#

class PID:
    """Discrete/user_defined PID controller."""

    def __init__(self, Epsilon=.01, Kp=1, Ki=1, Kd=1, Imax=300, Imin=-300):
        """Constructor."""

        # Maximum aceptable error.
        self.Ep = Epsilon

        # PID gains.
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd

        # Max cumulative error.
        self.Imax = Imax
        self.Imin = Imin

        # Initialize internal memory.
        self.reset()

    def process(self, error, derivative=None, integral=None):
        """Process the input error and return the three actuations terms: P, I, D.

            Allows to input both derivative and integral error values instead of using
            internal discrete calculation.

            This enables the caller to input external calculated values for both terms.
        """

        # Ignore errors inside epsilon range.
        if -self.Ep < error < self.Ep:
            return 0, 0, 0

        # Calculate Proportional term.
        P = self.Kp * error

        # Calculate Integral term.
        self._update_cumulative_error(error)
        if integral != None:
            I = self.Ki * integral
        else:
            I = self.Ki * self._integral(error)

        # Calculate Derivative term.
        if derivative != None:
            D = self.Kd * derivative
        else:
            D = self.Kd * self._derivative(error)
        self.last_error = error

        return P, I, D

    def reset(self):
        """Erase internal memory."""
        self.last_error = 0
        self.cumulative_error = 0

    def _integral(self, error):
        """Internal discrete integration method."""
        return self.cumulative_error

    def _derivative(self, error):
        """Internal discrete differentiation method."""
        return error - self.last_error

    def _update_cumulative_error(self, error):
        """Update and constraint cumulative error value."""
        self.cumulative_error += error
        if self.cumulative_error > self.Imax:
            self.cumulative_error = self.Imax
        elif self.cumulative_error < self.Imin:
            self.cumulative_error = self.Imin
