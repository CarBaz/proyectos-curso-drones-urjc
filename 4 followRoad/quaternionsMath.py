"""Some quaternion math utilities."""

from math import asin, atan2, pi

def quat2Pitch(qw, qx, qy, qz):
    rotateYa0=-2.0*(qx*qz - qw*qy)
    rotateY=0.0
    if(rotateYa0>=1.0):
        rotateY=pi/2.0
    elif(rotateYa0<=-1.0):
        rotateY=-pi/2.0
    else:
        rotateY=asin(rotateYa0)
    return rotateY

def quat2Yaw(qw, qx, qy, qz):
    rotateZa0=2.0*(qx*qy + qw*qz)
    rotateZa1=qw*qw + qx*qx - qy*qy - qz*qz
    rotateZ=0.0
    if(rotateZa0 != 0.0 and rotateZa1 != 0.0):
        rotateZ=atan2(rotateZa0,rotateZa1)
    return rotateZ

def quat2Roll(qw, qx, qy, qz):
    rotateXa0=2.0*(qy*qz + qw*qx)
    rotateXa1=qw*qw - qx*qx - qy*qy + qz*qz
    rotateX=0.0
    if(rotateXa0 != 0.0 and rotateXa1 !=0.0):
        rotateX=atan2(rotateXa0, rotateXa1)
    return rotateX

def quat2Actitude(qw, qx, qy, qz):
    return quat2Pitch(qw, qx, qy, qz)*(180/pi), quat2Yaw(qw, qx, qy, qz)*(180/pi), quat2Roll(qw, qx, qy, qz)*(180/pi)

