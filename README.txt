﻿Proyectos del "Curso Superior en Programación de Drones, Universidad Rey Juan Carlos".
    http://www.urjc.es/estudios/titulos-propios/888-curso-superior-universitario-en-programacion-de-drones

Las librerías, código base y entornos de desarrollo y simulación no incluidos,
Pueden obtenerse en:
    http://jderobot.org/Programacion-de-drones

  ------------------------------------------------------------------------------

Projects for the "Higher Degree Course in Drone Programming, Universidad Rey Juan Carlos".
    http://www.urjc.es/estudios/titulos-propios/888-curso-superior-universitario-en-programacion-de-drones

Required libraries, base code, develop and simulation environments not included here,
You may retrieve them at:
    http://jderobot.org/Programacion-de-drones

  ------------------------------------------------------------------------------

UNDER CONSTRUCTION...
    Contenidos:
        Documento memoria.      DONE
        Videos demostración.    DONE
        Capturas de pantalla.   DONE
  ------------------------------------------------------------------------------


  ------------------------------------------------------------------------------
